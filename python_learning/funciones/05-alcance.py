# No utilizar variables globales para una buena practica

saludo = "Hola global"

# saludo = 25


def saludar():
    global saludo  # Si es necesario usar una variable global
    saludo = "Hola Mundo"


def saludaChanchito():
    saludo = "Hola Chanchito"
    print(saludo)


# resultado1 = saludo + 3
# print(resultado1)
# saludar()
# resultado2 = saludo + 3
# print(resultado2)

print(saludo)
saludar()
print(saludo)
